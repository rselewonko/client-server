**Aplikacja Client - Server**


Projekt opierający się na socketach, prezentujący komunikacje clienta z serverem.


**Uruchomienie:**
1. Clone Repo
2. Ustawić ścieżke lokalną / zmienną środowiskową w zmiennej "path" klasy DataBase() w pliku database.py 
3. Uruchomić server.py
4. Uruchomić client.py 
5. Logowanie po stronie clienta (dane do usera testowego):
    TestUser:123test
6. Komenda "help" - wyświetla możliwe akcje w aplikacji
