import psycopg2 as ps2
from tabulate import tabulate
from config import db_user, password, port, host, database


class DataBase:
    def __init__(self):
        self.db_user = db_user
        self.password = password
        self.host = host
        self.port = port
        self.database = database
        self.connection = ps2.connect(
            user=self.db_user,
            password=self.password,
            host=self.host,
            port=self.port,
            database=self.database,
        )

    def show_data(self, query):
        with self.connection:
            with self.connection.cursor() as cursor:
                cursor.execute(query)
                records = cursor.fetchall()
                headers = [head[0] for head in cursor.description]
                table = tabulate(records, headers, tablefmt="psql")
                return table

    def check_user_in_DB(self, username):
        """return user tuple if user exists"""
        with self.connection:
            with self.connection.cursor() as cursor:
                query = "SELECT * FROM users WHERE username = %s"
                cursor.execute(query, (username,))
                record = cursor.fetchone()
                print(cursor.statusmessage)
                return record

    # object_data = user dict (from User class)
    def add_user_to_DB(self, object_data):
        if self.check_user_in_DB(object_data["username"]) is None:
            with self.connection:
                with self.connection.cursor() as cursor:
                    query = """INSERT INTO users (username, password, rights)
                            VALUES (%s, %s, %s)"""
                    data_to_insert = (
                        object_data["username"],
                        object_data["password"],
                        object_data["rights"],
                    )
                    cursor.execute(query, data_to_insert)
                    print(cursor.statusmessage)
                    return True
        return None

    def delete_user_from_DB(self, username):
        if self.check_user_in_DB(username) is not None:
            with self.connection:
                with self.connection.cursor() as cursor:
                    query = "DELETE FROM users WHERE username = %s"
                    cursor.execute(query, (username,))
                    print(cursor.statusmessage)
                    return True
        return None

    def password_reset(self, username, new_pass):
        if self.check_user_in_DB(username) is not None:
            with self.connection:
                with self.connection.cursor() as cursor:
                    query = "UPDATE users SET password = %s WHERE username = %s"
                    cursor.execute(query, (new_pass, username))
                    print(cursor.statusmessage)
                    return True
        return None

    def check_user_credentials(self, username, password):
        """checking user credentials and return user rights level"""

        record = self.check_user_in_DB(username)
        if record is not None and record[2] == password:
            return record[3]
        return None

    def send_direct_msg(self, receiver, sender, text):
        if self.check_user_in_DB(receiver) is not None:
            with self.connection:
                with self.connection.cursor() as cursor:
                    query = """INSERT INTO msgbox (sender, receiver, msg_text)
                            VALUES (%s, %s, %s)"""
                    cursor.execute(query, (sender, receiver, text[:255].strip()))
                    print(cursor.statusmessage)
                    return "OK"
        return None

    def read_msg_box(self, username):
        with self.connection:
            with self.connection.cursor() as cursor:
                query = """SELECT sender, msg_text FROM msgbox
                        WHERE receiver = %s"""
                cursor.execute(query, (username,))
                print(cursor.statusmessage)
                msg = {}
                for tup in cursor.fetchall():
                    msg[tup[0]] = tup[1]
                return msg

    def clear_msg_box(self, username):
        if self.check_user_in_DB(username) is not None:
            with self.connection:
                with self.connection.cursor() as cursor:
                    query = "DELETE FROM msgbox WHERE receiver = %s"
                    cursor.execute(query, (username,))
                    print(cursor.statusmessage)
                    return True
        return None
